<!doctype html>
<html lang="pt">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Vaquinha</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="./css/estilo.css">
</head>
<body>

    <header>
        <div>
            <h1>
                Projeto Itamú!
            </h1>
        </div>
        
        <!-- <div class="botoes">
                <a id="botaologin" href="#" class="btn btn-default btn-lg active" role="button">Login</a>
                <a id="botaologar" href="#" class="btn btn-default btn-lg active" role="button">Cadastrar</a>
        </div> -->
        
    </header>

    <!--Carousel Wrapper-->
    <div id="carousel-example-2" class="carousel slide carousel-fade" data-ride="carousel" data-interval="3000">
        <!--Indicators-->
        <ol class="carousel-indicators">
            <li data-target="#carousel-example-2" data-slide-to="0" class="active"></li>
            <li data-target="#carousel-example-2" data-slide-to="1"></li>
            <li data-target="#carousel-example-2" data-slide-to="2"></li>
        </ol>
        <!--/.Indicators-->
        <!--Slides-->
        <div class="carousel-inner" role="listbox">
            <div class="carousel-item active">
                <div class="txt-fixed1">
                        <p>Faça a criação da Vaquinha!</p>
                </div>
                <div class="view">
                    <img class="d-block w-100"" src="./imagens/vaca1.png" alt="First slide">
                    <div class="mask rgba-black-light"></div>
                </div>
                <div class="carousel-caption">
                    <h3 class="h3-responsive"></h3>
                    <p></p>
                </div>
            </div>
            <div class="carousel-item">
                <!--Mask color-->
                <div class="txt-fixed2">
                        <p>Inclua os amigos que irão participar!</p>
                </div>
                <div class="view">
                    <img class="d-block w-100" src="./imagens/vaca2.png" alt="Second slide">
                    <div class="mask rgba-black-strong"></div>
                </div>
                <div class="carousel-caption">
                    <h3 class="h3-responsive"></h3>
                    <p></p>
                </div>
            </div>
            <div class="carousel-item">
                <!--Mask color-->
                <div class="txt-fixed3">
                        <p>Agora é só esperar!</p>
                </div>
                <div class="txt-fixed4">
                        <p>Sem dor de cabeça e muuuuuuito fácil!</p>
                </div>
                <div class="view">
                    <img class="d-block w-100" src="./imagens/vaca3.png" alt="Third slide">
                    <div class="mask rgba-black-slight"></div>
                </div>
                <div class="carousel-caption">
                    <h3 class="h3-responsive"></h3>
                    <p></p>
                </div>
            </div>
        </div>
        <!--/.Slides-->
        <!--Controls-->
        <a class="carousel-control-prev" href="#carousel-example-2" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carousel-example-2" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
        <!--/.Controls-->
    </div>
    <!--/.Carousel Wrapper-->

    
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>

</body>
</html>







header {
    display: block;
    margin-left: auto;
    margin-right: auto;
    padding: 3px;
    background-color: darkolivegreen;
    border: 5px solid black;
    border-bottom: 1px solid black;
    color: darkblue;
    font-family: 'Franklin Gothic Medium', 'Arial Narrow', Arial, sans-serif;
    
}

.d-block  {
    display: block;
    margin-left: auto;
    margin-right: auto;
    padding: 3px;
    text-align: center;
    background-color: black;
    border: 1px solid black;
    height: 594px;
}

.txt-fixed1 {
    position:absolute;
    top:30%;
    left:40%;
    font-size: 60px;
}

.txt-fixed2 {
    position:absolute;
    top:30%;
    left:36%;
    font-size: 53px;
}

.txt-fixed3 {
    position:absolute;
    top:19%;
    left:40%;
    font-size: 60px;
}

.txt-fixed4 {
    position:absolute;
    top:34%;
    left:40%;
    font-size: 40px;
}

.botoes {
    text-align: right; 
}